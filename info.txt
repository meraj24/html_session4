Git global setup

git config --global user.name "Meraj alam"
git config --global user.email "mrjalm1@gmail.com"

Create a new repository

git clone https://gitlab.com/meraj24/html_session4.git
cd html_session4
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/meraj24/html_session4.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/meraj24/html_session4.git
git push -u origin --all
git push -u origin --tags